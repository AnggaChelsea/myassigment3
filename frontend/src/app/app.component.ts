import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  export class ProfileComponent implements OnInit {
    readonly ROOT_URL = 'https://localhost:3000/users'
    users:any
  
    constructor(private http: HttpClient) { }
  
    getUsers(){
      this.users = this.http.get(this.ROOT_URL + '/users')
    }
  
}
