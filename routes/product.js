const router = require('express').Router();
const Product = require('../controllers/Product');

router.post('/:id', Product.post);

module.exports = router;