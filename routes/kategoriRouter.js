const router = require('express').Router();
const userKategori = require('../controllers/KategotyController')

router.patch('/:id', userKategori.patch);

module.exports = router