const router = require('express').Router();
const authentication = require('../middlewares/authentication');
const userRoutes = require('./user');
const alamtrouter = require('./alamat')
const Kategori = require('./kategoriRouter');
const Product = require('./product')
const masukkeranjang = require('./keranjangRouter')


const errorHandler = require('../middlewares/errorHandler');


router.use('/users', userRoutes);
router.use(authentication);
router.use('/kategori', Kategori);
router.use('/alamat', alamtrouter);
router.use('/product', Product);
router.post('/keranjang', masukkeranjang);
router.use(errorHandler);

module.exports = router;