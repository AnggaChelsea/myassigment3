const mongoose = require('mongoose');


const ProductSchema = new mongoose.Schema({

  _kategorieId:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Kategory',
    required: true
  },

  namabarang: {
    type: String,
    required: true
  },
  tumbnail: {
    type: String,
    required: true
  },
  gambar: {
    type: String,
    required: true
  },
  deskripsi: {
    type: String,
    required: true
  },
  harga: {
    type: Number,
    required: true
  },
  jumlah: {
    type: Number,
    required: true
  },
  deskripsisingkat: {
    type: String,
    required: true
  },
  published: {
    type: Number,
    default: Date.now(),
  },
});

const Product = mongoose.model('Product', ProductSchema);
module.exports = Product;