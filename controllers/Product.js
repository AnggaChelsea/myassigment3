const Productjs = require('../models/Product');
const Kategory = require('../models/Kategory')

class Product {
  static post(req, res, next) {
    const {
      namabarang,
      tumbnail,
      gambar,
      deskripsi,
      harga,
      jumlah,
      deskripsisingkat
    } = req.body;
    const product = new Productjs({
      _kategoriId: req.params.kategoriId,
      namabarang,
      tumbnail,
      gambar,
      deskripsi,
      harga,
      jumlah,
      deskripsisingkat
    })
    product.save()
    .then((product) =>{
      res.status(201).json({message:'succes product added successfully', data: product});
    })
    .catch(next)
  }

  // static post(req, res, next) {
  //   const { kategoriId } = req.body;
  //   Productjs.findOne({_id: req.params.id})
  //   .then((product) =>{

  //   })
  // }

}

module.exports = Product;