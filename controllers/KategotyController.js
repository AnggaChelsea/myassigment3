const Kategori = require('../models/Kategory');

class KategoryController {
  static patch(req, res, next) {
    console.log(req._id);
    const {
      kategori
    } = req.body;
    const category = new Kategori({
      _productId: req.params._id,
      kategori,
    })
    category.save()
      .then(function (category) {
        res.status(201).json({
          success: true,
          data: category
        });
      })
      .catch(next);
  }
}

module.exports = KategoryController;